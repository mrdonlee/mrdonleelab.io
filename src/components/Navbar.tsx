const Navbar = () => {
    return (
        <nav className="my-2 bg-gray-800 rounded-lg">
            <ul className="px-0">
                <li className="p-2 px-6 hover:bg-blue-900 duration-300 rounded-lg">
                    <a href="#" className="flex items-center text-gray-300 hover:text-white">
                        <svg className="w-4 h-4 mr-2" fill="none" stroke="currentColor" stroke-width="0.1"
                            viewBox="0 0 24 24"
                            xmlns="http://www.w3.org/2000/svg">
                            <path fill="currentColor"
                                d="M13 9V3h8v6zM3 13V3h8v10zm10 8V11h8v10zM3 21v-6h8v6zm2-10h4V5H5zm10 8h4v-6h-4zm0-12h4V5h-4zM5 19h4v-2H5zm4-2" />
                        </svg>
                        Home
                    </a>
                </li>
                <li className="p-2 px-6 hover:bg-blue-900 duration-300 rounded-lg">
                    <a href="#" className="flex items-center text-gray-300 hover:text-white">
                        <svg className="w-4 h-4 mr-2" fill="none" stroke="currentColor" stroke-width="0.1"
                            viewBox="0 0 16 16"
                            xmlns="http://www.w3.org/2000/svg">
                            <path fill="currentColor" fill-rule="evenodd"
                                d="m9.5 14.5l-6-2.5V4l6-2.5zm-6.885-1.244A1 1 0 0 1 2 12.333V3.667a1 1 0 0 1 .615-.923L8.923.115A1.5 1.5 0 0 1 11 1.5V2h1.25c.966 0 1.75.783 1.75 1.75v8.5A1.75 1.75 0 0 1 12.25 14H11v.5a1.5 1.5 0 0 1-2.077 1.385zM11 12.5h1.25a.25.25 0 0 0 .25-.25v-8.5a.25.25 0 0 0-.25-.25H11z" clip-rule="evenodd" />
                        </svg>
                        Projects
                    </a>
                </li>
                <li className="p-2 px-6 hover:bg-blue-900 duration-300 rounded-lg">
                    <a href="#" className="flex items-center text-gray-300 hover:text-white">
                        <svg className="w-4 h-4 mr-2" fill="none" stroke="currentColor" stroke-width="0.1"
                            viewBox="0 0 24 24"
                            xmlns="http://www.w3.org/2000/svg">
                            <path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M16 6h3a1 1 0 0 1 1 1v11a2 2 0 0 1-4 0V5a1 1 0 0 0-1-1H5a1 1 0 0 0-1 1v12a3 3 0 0 0 3 3h11M8 8h4m-4 4h4m-4 4h4" />
                        </svg>
                        Posts
                    </a>
                </li>
                <li className="p-2 px-6 hover:bg-blue-900 duration-300 rounded-lg">
                    <a href="#" className="flex items-center text-gray-300 hover:text-white">
                        <svg className="w-4 h-4 mr-2" fill="none" stroke="currentColor" stroke-width="0.1"
                            viewBox="0 0 24 24"
                            xmlns="http://www.w3.org/2000/svg">
                            <path fill="currentColor"
                                d="M11 9h2V7h-2m1 13c-4.41 0-8-3.59-8-8s3.59-8 8-8s8 3.59 8 8s-3.59 8-8 8m0-18A10 10 0 0 0 2 12a10 10 0 0 0 10 10a10 10 0 0 0 10-10A10 10 0 0 0 12 2m-1 15h2v-6h-2z" />
                        </svg>
                        About
                    </a>
                </li>
                <li className="p-2 px-6 hover:bg-blue-900 duration-300 rounded-lg">
                    <a href="#" className="flex items-center text-gray-300 hover:text-white">
                        <svg className="w-4 h-4 mr-2" fill="none" stroke="currentColor" stroke-width="0.1"
                            viewBox="0 0 24 24"
                            xmlns="http://www.w3.org/2000/svg">
                            <path fill="currentColor"
                                d="M19 19H5V5h14m0-2H5a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V5a2 2 0 0 0-2-2m-2.5 13.25c0-1.5-3-2.25-4.5-2.25s-4.5.75-4.5 2.25V17h9M12 12.25A2.25 2.25 0 0 0 14.25 10A2.25 2.25 0 0 0 12 7.75A2.25 2.25 0 0 0 9.75 10A2.25 2.25 0 0 0 12 12.25" />
                        </svg>
                        Contact
                    </a>
                </li>
            </ul>
        </nav>
    );
};

export default Navbar;