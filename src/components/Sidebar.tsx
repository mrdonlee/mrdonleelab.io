import Social from "./Social";
import Navbar from "./Navbar";
import Details from "./Details";

const Sidebar = () => {
    return (
        <aside className="w-72 m-4 bg-gray-900 p-2 rounded-xl drop-shadow">
            <Details />
            <Navbar />
            <Social />
        </aside>
    );
};

export default Sidebar;