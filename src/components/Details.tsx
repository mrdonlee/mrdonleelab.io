import dp from '/images/display_picture.jpg';

const Details = () => {
    return (
        <section className="mb-4">
            <img className="p-4 rounded-3xl" src={dp} alt='display picture' />
            <h1 className="text-2xl font-bold text-center mb-1">Nidharshanen Selliah</h1>
            <h2 className="text-l font-bold text-center mx-8 p-1 px-2 bg-gray-800 rounded-lg">Cyber Security Student</h2>
        </section>
    );
};

export default Details;